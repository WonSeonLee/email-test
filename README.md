### Quick summary
jsp email send example

### Version
1.0

### Summary of set up
tomcat 6.0 or higher

### Dependencies
velocity template library

### How to run tests
1) install jsp, java file, load index.html 
2) input gmail id and password
3) input mail title, subject, content and submit
4) check your email