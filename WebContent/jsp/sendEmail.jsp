<%@page import="org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.apache.velocity.runtime.RuntimeConstants"%>
<%@page import="org.apache.velocity.Template"%>
<%@page import="java.io.StringWriter"%>
<%@page import="org.apache.velocity.VelocityContext"%>
<%@page import="org.apache.velocity.app.VelocityEngine"%>
<%@page import="auth.MyAuthentication"%>
<%@page import="java.util.Date" %>
<%@page import="java.util.Properties" %>
<%@page import="javax.mail.Authenticator" %>
<%@page import="javax.mail.Message" %>
<%@page import="javax.mail.MessagingException" %>
<%@page import="javax.mail.PasswordAuthentication" %>
<%@page import="javax.mail.Session" %>
<%@page import="javax.mail.internet.AddressException" %>
<%@page import="javax.mail.internet.InternetAddress" %>
<%@page import="javax.mail.internet.MimeMessage" %>
<% request.setCharacterEncoding("UTF-8"); %>
<%	
	String from = request.getParameter("from");
	String to = request.getParameter("to");
	String subject = request.getParameter("subject");
	String content = request.getParameter("content");

	String id = request.getParameter("id");
	String pw = request.getParameter("pw");
	
	Properties p = System.getProperties();
	p.put("mail.smtp.starttls.enable", "true");     // gmail은 무조건 true 고정
	p.put("mail.smtp.host", "smtp.gmail.com");      // smtp 서버 주소
	p.put("mail.smtp.auth","true");                 // gmail은 무조건 true 고정
	p.put("mail.smtp.port", "587");                 // gmail 포트
	   
	
	
	Authenticator auth = new MyAuthentication(id, pw);
	 
	//session 생성 및  MimeMessage생성
	Session sessiona = Session.getDefaultInstance(p, auth);
	MimeMessage msg = new MimeMessage(sessiona);
	 
	try {
		
	    //편지보낸시간
	    msg.setSentDate(new Date());
	     
	    InternetAddress iaFrom = new InternetAddress(from);
	     
	    // 이메일 발신자
	    msg.setFrom(iaFrom);
	     
	    // 이메일 수신자
	    InternetAddress iaTo = new InternetAddress(to);
	    msg.setRecipient(Message.RecipientType.TO, iaTo);
	     
	    // 이메일 제목
	    msg.setSubject(subject, "UTF-8");
	    
	    StringWriter w = new StringWriter();
	     
	    VelocityEngine ve = new VelocityEngine();
	    ve.setProperty("input.encoding", "UTF-8");
	    ve.setProperty("output.encoding", "UTF-8");
	    ve.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
	    ve.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());
	    ve.init();

	    Template t = ve.getTemplate("templates/template_sample.vm");
	    VelocityContext context = new VelocityContext();

	    t.merge(context, w); 

	    // 이메일 내용
	    msg.setText(w.toString(), "UTF-8"); 

	    // 이메일 헤더
	    msg.setHeader("content-Type", "text/html");

	    //메일보내기
	    javax.mail.Transport.send(msg);
	     
	}catch (AddressException addr_e) {
	    addr_e.printStackTrace();
	}catch (MessagingException msg_e) {
	    msg_e.printStackTrace();
	}
%>